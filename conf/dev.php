<?php

$config = [
    'git_repo_url'        => 'https://gitee.com/myDcool/test_deploy.git', //git仓库地址
    'ignore_files'        => ['.git/', 'README.en.md'],  //忽略的文件
    'cache_files'         => [], //缓存目录,文件
    'target_path_symlink' => 'D:/server/code/test2', //软连接, 一般是网站的根目录, 脚本部署完毕后会创建一个新的软连接指向 target_path
    'target_path'         => 'D:/server/code/test1/dev', //最新代码最终部署到的目录, 最后不要有“/”,
    'cmd' => [], //代码拷贝完成后，执行的命令，比如：创建软连接
];